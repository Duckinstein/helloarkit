//
//  SpaceShip.swift
//  HelloARKit
//
//  Created by rightmeow on 7/9/17.
//  Copyright © 2017 Duckensburg. All rights reserved.
//

import Foundation
import ARKit

class SpaceShip: SCNNode {

    var modelName: String = ""
    var fileExtension: String = ""
    var thumbImageFileName: String = ""
    var title: String = ""
    var modelLoaded: Bool = false

    func loadModel(completion: (Bool) -> Void) {
        guard let scene = SCNScene(named: "\(modelName).\(fileExtension)", inDirectory: "Model.scnassets/\(modelName)") else { return }
        let wrapperNode = SCNNode()
        for child in scene.rootNode.childNodes {
            child.geometry?.firstMaterial?.lightingModel = .physicallyBased
            child.movabilityHint = .movable
            wrapperNode.addChildNode(child)
        }
        self.addChildNode(wrapperNode)
        modelLoaded = true
        completion(modelLoaded)
    }

    // Lifecycle

    init(modelName: String, fileExtension: String, thumbImageFileName: String, title: String) {
        super.init()
        self.modelName = modelName
        self.fileExtension = fileExtension
        self.thumbImageFileName = thumbImageFileName
        self.title = title
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
